export const app = {
    a: "Ok",
    b: "Fechar",
    c: "Salvar",
    f: "seg",
    g: "ter",
    h: "qua",
    i: "qui",
    j: "sex",
    k: "sab",
    l: "dom",
    m: "Nenhum Item",
}


export const homePage = {
    a: "Peito",
    b: "Biceps",
    c: "Triceps",
    d: "Costa",
    e: "Ombro",
    f: "Perna",
    g: "Abdômen"
}

export const myTrainingsPage = {
    a: "Meus Treinos",
    b: "Período",
    c: "Frequência"
}

export const addTrainingPage = {
    a: "Titulo",
    b: "Inicio",
    c: "Fim",
    d: "Frequência",
    e: "Exercicios",
    f: "segunda",
    g: "terça",
    h: "quarta",
    i: "quinta",
    j: "sexta",
    k: "sabado",
    l: "domingo",
    m: "Adicionar Treino",
    n: "Nenhum Item",
    o: "Atenção",
    p: "Deseja excluir o item selecionado?"
}

export const trainingDetailPage = {
    a: "Adicionar Treino",
}

export const addExercisePage = {
    a: "Série X Repetições",
    b: "Série",
    c: "Repetições",
    d: "Peso (Kg)",
}

export const schedulePage = {
    a: "Janeiro",
    b: "Fevereiro",
    c: "Março",
    d: "Abril",
    e: "Maio",
    f: "Junho",
    g: "Julho",
    h: "Agosto",
    i: "Setembro",
    j: "Outubro",
    k: "Novembro",
    l: "Dezembro",
    m: "Agenda"
}

export const goalsPage = {
    a: "Objetivo"
}

export const musclePage = {
    a: "Músculos"
}