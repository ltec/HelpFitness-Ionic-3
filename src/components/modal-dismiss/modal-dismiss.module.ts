import { NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { ModalDismissComponent } from './modal-dismiss';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
	declarations: [
        ModalDismissComponent
    ],
	imports: [
       IonicPageModule.forChild(ModalDismissComponent)
    ],
    exports: [ModalDismissComponent],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
    })
export class ModalDismissModule {}