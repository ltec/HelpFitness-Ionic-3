import { Component } from '@angular/core';
import { ViewController, NavController } from 'ionic-angular';
import { app } from './../../config/portugues';
import { PageSettingsProvider } from './../../providers/page-settings/page-settings';

@Component({
  selector: 'modal-dismiss',
  templateUrl: 'modal-dismiss.html'
})
export class ModalDismissComponent {

  language: any;
  constructor(private navCtrl: NavController,
    public viewCtrl: ViewController,
    private pageSettingsProvider: PageSettingsProvider) {
    this.language = app;
  }

  dismiss() {
    if (this.pageSettingsProvider.footer && this.navCtrl.length() == 1)
      this.pageSettingsProvider.setPageFooter(false)
        .then(() => this.viewCtrl.dismiss())
    else
      this.viewCtrl.dismiss();
  }


}
