import { NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CatalogComponent } from './catalog';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
	declarations: [
        CatalogComponent
    ],
	imports: [
       IonicPageModule.forChild(CatalogComponent)
    ],
    exports: [CatalogComponent],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
    })
export class CatalogModule {}
