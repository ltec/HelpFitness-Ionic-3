import { Component, Input, EventEmitter, Output, ViewChild, AfterViewInit } from '@angular/core';
import { Slides } from 'ionic-angular';

@Component({
  selector: 'catalog',
  templateUrl: 'catalog.html'
})
export class CatalogComponent implements AfterViewInit {

  @Input('source') source: any[];
  @Input('timeout') timeout: number = 4000;
  @Input() rtl: boolean = true;
  @Output() click = new EventEmitter();
  @ViewChild(Slides) slides: Slides;

  constructor() {

  }

  ngAfterViewInit() {
  }

  getItem(item) {
    this.click.emit({ item: item })
  }

}
