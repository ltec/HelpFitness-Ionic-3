import { NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { HeaderComponent } from './header';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
	declarations: [
        HeaderComponent
    ],
	imports: [
       IonicPageModule.forChild(HeaderComponent)
    ],
    exports: [HeaderComponent],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
    })
export class HeaderModule {}
