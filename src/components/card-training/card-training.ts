import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Slides } from 'ionic-angular';
import { app } from './../../config/portugues';

@Component({
  selector: 'card-training',
  templateUrl: 'card-training.html'
})
export class CardTrainingComponent {

  @Input('source') source: any[];
  @Output() updateTraining = new EventEmitter();
  @Output() deleteTraining = new EventEmitter();
  language: any;

  constructor() {
    this.language = app;
  }

  updateTrainingEvent(training, index) {
    this.updateTraining.emit({ training: training, index: index })
  }

  deleteTrainingEvent(index) {
    this.updateTraining.emit({ index: index })
  }

}
