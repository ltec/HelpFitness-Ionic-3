import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CardTrainingComponent } from './card-training';
import { IonicPageModule } from 'ionic-angular';
import { DaysInWeekPipeModule } from '../../pipes/days-in-week/days-in-week.module';
import { SerieXrepetitionsPipeModule } from './../../pipes/serie-xrepetitions/serie-xrepetitions.module';

@NgModule({
    declarations: [
        CardTrainingComponent
    ],
    imports: [
        DaysInWeekPipeModule,
        SerieXrepetitionsPipeModule,
        IonicPageModule.forChild(CardTrainingComponent)
    ],
    exports: [CardTrainingComponent],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class CardTrainingModule { }
