import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { app } from './../../config/portugues';
import { PageSettingsProvider } from './../../providers/page-settings/page-settings';

@Component({
  selector: 'nav-pop-to-root',
  templateUrl: 'nav-pop-to-root.html'
})
export class NavPopToRootComponent {

  language: any;
  constructor(private navCtrl: NavController,
    private pageSettingsProvider: PageSettingsProvider) {
    this.language = app;
  }

  close() {
    this.navCtrl.remove(2, this.navCtrl.length() - 2);
  }
}
