import { NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { NavPopToRootComponent } from './nav-pop-to-root';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
	declarations: [
        NavPopToRootComponent
    ],
	imports: [
       IonicPageModule.forChild(NavPopToRootComponent)
    ],
    exports: [NavPopToRootComponent],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
    })
export class NavPopToRootModule {}