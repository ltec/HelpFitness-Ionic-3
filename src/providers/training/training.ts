import { Injectable } from '@angular/core';
import * as data from './../../data/data.json';
import { DatabaseProvider } from './../database/database';

@Injectable()
export class TrainingProvider {

  data: any;
  myTrainings: any[] = [];
  constructor(private databaseProvider: DatabaseProvider) {
    this.data = (<any>data);
  }

  getTrainingsAll(goal: number, muscles: any[]){
    return new Promise((resolve, reject) => {
      try {

        let data = this.data.goals
          .find(x => x.key == goal)
          .muscles
          .filter(x => muscles.indexOf(x.key) > -1)

        resolve(data);
      } catch (error) {
        console.log(error);
        reject(error)
      }
    });
  }

  getTrainingsPeito() {
    return new Promise((resolve, reject) => {
      try {

        let data = this.data.objetivos
          .find(x => x.id == 1)
          .musculos
          .find(x => x.id == 1)
          .exercises;

        resolve(data);
      } catch (error) {
        console.log(error);
        reject(error)
      }
    });
  }

  setMyTrainings(item) {
    return new Promise((resolve, reject) => {
      this.databaseProvider.setItem("myTrainings", item)
        .then(() => resolve())
        .catch(error => reject(error))
    })
  }

  getMyTrainings() {
    return new Promise((resolve, reject) => {
      this.databaseProvider.getItem("myTrainings")
        .then((itens: any[]) => {
          this.myTrainings = itens;
          resolve(itens)
        })
        .catch(error => {
          reject(error);
        })
    })
  }

  public dateReviver(key, value) {
    if (typeof value === 'string') {
      var a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
      if (a) {
        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]));
      }
    }
    return value;
  };

}
