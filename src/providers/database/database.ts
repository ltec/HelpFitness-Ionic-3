import { NativeStorage } from '@ionic-native/native-storage';
import { Injectable } from '@angular/core';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

  constructor(private nativeStorage: NativeStorage) {

  }

  setItem(key, item) {
    return new Promise((resolve, reject) => {
      this.nativeStorage.setItem(key, item)
        .then(() => resolve())
        .catch(error => reject(error));
    })
  }

  getItem(key) {
    return new Promise((resolve, reject) => {
      this.nativeStorage.getItem(key)
        .then(item => {
          resolve(item)
        })
        .catch(error => reject(error));
    })
  }

}
