import { Injectable } from '@angular/core';

@Injectable()
export class PageSettingsProvider {

  footer: boolean = false;
  pilhe: number = 0;

  setPageFooter(val) {
    return new Promise((resolve, reject) => {
      this.footer = val;
      resolve();
    });
  }


  addPagePilhe() {
    return new Promise((resolve, reject) => {
      this.pilhe++;
      resolve();
    });
  }

  removePagePilhe() {
    return new Promise((resolve, reject) => {
      this.pilhe--;
      resolve();
    });
  }
}
