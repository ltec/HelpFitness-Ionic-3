import { AlertController } from 'ionic-angular';
import { Injectable } from '@angular/core';

@Injectable()
export class MessageProvider {

  constructor(private alertCtrl: AlertController) { }

  Message(titulo: string, subTitulo: any) {
    if (typeof (subTitulo) === 'object')
      subTitulo = JSON.stringify(subTitulo);

    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: subTitulo,
      buttons: ['OK']
    });
    alert.present();
  }

  errorMessage(message: any) {
    if (typeof (message) === 'object')
      message = JSON.stringify(message);

    let alert = this.alertCtrl.create({
      title: 'Error!',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  confirm(titulo: string = '', Message: string = '') {
    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create({
        title: titulo,
        message: Message,
        buttons: [
          {
            text: 'Fechar',
            role: 'cancel',
            handler: () => {
              reject();
            }
          },
          {
            text: 'OK',
            handler: () => {
              resolve();
            }
          }
        ]
      });
      alert.present();
    })

  }

}
