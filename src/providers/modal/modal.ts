import { Injectable } from '@angular/core';
import { ModalController, NavParams } from 'ionic-angular';

@Injectable()
export class ModalProvider {

  constructor(private modalCtrl: ModalController) {}

  open(page: string, param: any = {}) {
    let profileModal = this.modalCtrl.create(page, param);
    profileModal.present();
  }



}
