import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HeaderModule } from './../components/header/header.module';

import { LoadingProvider } from './../providers/loading/loading';
import { MessageProvider } from './../providers/message/message';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ModalProvider } from '../providers/modal/modal';
import { PageSettingsProvider } from '../providers/page-settings/page-settings';
import { DatabaseProvider } from '../providers/database/database';
import { NativeStorage } from '@ionic-native/native-storage';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HeaderModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LoadingProvider,
    MessageProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ModalProvider,
    PageSettingsProvider,
    DatabaseProvider,
    NativeStorage
  ]
})
export class AppModule {}
