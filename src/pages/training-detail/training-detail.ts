import { Component } from '@angular/core';
import { IonicPage, NavParams, NavController } from 'ionic-angular';
import { PageSettingsProvider } from './../../providers/page-settings/page-settings';
import { trainingDetailPage } from './../../config/portugues';

/**
 * Generated class for the TrainingDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-training-detail',
  templateUrl: 'training-detail.html',
})
export class TrainingDetailPage {

  item: any;
  language: any;
  pageFooterShow: boolean = false;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private pageSettingsProvider: PageSettingsProvider) {
    this.item = this.navParams.get("item");
    this.language = trainingDetailPage;

    this.pageFooterShow = this.pageSettingsProvider.footer;
  }

  ionViewDidLoad() {

  }

  addExercise() {
    this.navCtrl.push("AddExercisePage", { item: this.item })
  }

}
