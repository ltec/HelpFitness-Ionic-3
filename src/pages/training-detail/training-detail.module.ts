import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainingDetailPage } from './training-detail';
import { HeaderModule } from './../../components/header/header.module';
import { NavPopToRootModule } from './../../components/nav-pop-to-root/nav-pop-to-root.module';

@NgModule({
  declarations: [
    TrainingDetailPage,
  ],
  imports: [
    HeaderModule,
    NavPopToRootModule,
    IonicPageModule.forChild(TrainingDetailPage),
  ],
})
export class TrainingDetailPageModule {}
