import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoalsPage } from './goals';
import { HeaderModule } from './../../components/header/header.module';

@NgModule({
  declarations: [
    GoalsPage,
  ],
  imports: [
    HeaderModule,
    IonicPageModule.forChild(GoalsPage),
  ],
})
export class GoalsPageModule {}
