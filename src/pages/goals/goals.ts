import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { goalsPage } from '../../config/portugues';



@IonicPage()
@Component({
  selector: 'page-goals',
  templateUrl: 'goals.html',
})
export class GoalsPage {

  source = [
    {key: 1, goal: 'Hipertrofia'},
    {key: 2, goal: 'AumentoForca'}, 
    {key: 3, goal: 'Definicao'},
    {key: 4, goal: 'PercaDePeso'},
    {key: 5, goal: 'CondicionamentoFisico'}
  ];
  language: any; 

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.language = goalsPage;
  }

  ionViewDidLoad() {
  }

  select(key){
    this.navCtrl.push("MusclesPage")
  }

}
