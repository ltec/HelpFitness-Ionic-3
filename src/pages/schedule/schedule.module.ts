import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SchedulePage } from './schedule';
import { ScheduleProvider } from './../../providers/schedule/schedule';
import { TrainingProvider } from './../../providers/training/training';
import { CardTrainingModule } from './../../components/card-training/card-training.module';
import { HeaderModule } from './../../components/header/header.module';

@NgModule({
  declarations: [
    SchedulePage,
  ],
  imports: [
    HeaderModule,
    CardTrainingModule,
    IonicPageModule.forChild(SchedulePage),
  ],
  providers: [
    ScheduleProvider,
    TrainingProvider
  ]
})
export class SchedulePageModule { }
