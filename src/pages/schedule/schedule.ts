import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Slides } from 'ionic-angular';
import { ScheduleProvider } from './../../providers/schedule/schedule';
import { app, schedulePage } from './../../config/portugues';
import { TrainingProvider } from './../../providers/training/training';
import { Training } from '../../model/training';
import { MessageProvider } from './../../providers/message/message';

@IonicPage()
@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
})
export class SchedulePage {
  @ViewChild('headerSlides') headerSlides: Slides;
  @ViewChild('scheduleSlides') scheduleSlides: Slides;

  month: any[];
  active: number = new Date().getDate();
  language: any;
  months: any[];
  title: string;
  titleWeek: string;
  currentDate: Date = new Date();
  slideInCalendar: boolean = false;
  slideOutCalendar: boolean = false;
  weekDay: any[];
  headerInit: boolean = true;
  scheduleInit: boolean = true;
  slideInit: boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private scheduleProvider: ScheduleProvider,
    private trainingProvider: TrainingProvider,
    private messageProvider: MessageProvider,
    private events: Events) {
    this.language = app;
    this.language.schedulePage = schedulePage;
    this.months = [
      schedulePage.a,
      schedulePage.b,
      schedulePage.c,
      schedulePage.d,
      schedulePage.e,
      schedulePage.f,
      schedulePage.g,
      schedulePage.h,
      schedulePage.i,
      schedulePage.j,
      schedulePage.k,
      schedulePage.l
    ];

    this.weekDay = [
      this.language.l,
      this.language.f,
      this.language.g,
      this.language.h,
      this.language.i,
      this.language.j,
      this.language.k
    ];

    this.events.subscribe('training:add', () => {
      if (this.month){
        this.month = [];
        this.myTrainings(this.currentDate);
      }
    });
  }

  ionViewDidLoad() {
    this.scheduleProvider.getMonth(this.currentDate)
      .then((month: any) => {
        this.month = month;
        this.title = this.months[this.currentDate.getMonth()];
        this.titleWeek = this.months[this.currentDate.getMonth()];
        let dateRef = new Date();
        let date = new Date(dateRef.getFullYear(), dateRef.getMonth());
        let week = Math.ceil((dateRef.getDate() + date.getDay()) / 7);

        this.active = dateRef.getDay() + (week - 1) * 7;
      })
      .then(() => {
        this.myTrainings(this.currentDate)
          .then(() => console.log(this.month));
      })
  }

  ionViewDidLeave() {
    if (this.slideInOutCalendar){
      this.slideInit = true;
    }
    this.slideInCalendar = false;
    this.slideOutCalendar= false;
  }

  dayDetail(index) {
    this.active = index;
    this.scheduleSlides.slideTo(index, 500, false);
    this.headerSlides.slideTo(index, 500, false);
    this.titleWeek = this.months[this.month[index].date.getMonth()];
  }

  headerIndex(slides: Slides) {
    if (this.headerInit) {
      this.headerInit = false;
      return;
    }
    var index = slides.getActiveIndex();
    this.scheduleSlides.slideTo(index, 500, false);
    this.active = index;
    this.titleWeek = this.months[this.month[index].date.getMonth()];
  }

  scheduleIndex(slides: Slides) {
    if (this.scheduleInit) {
      this.scheduleInit = false;
      return;
    }
    var index = slides.getActiveIndex();
    this.headerSlides.slideTo(index, 500, false);
    this.active = index;
    this.titleWeek = this.months[this.month[index].date.getMonth()];
  }

  slideInOutCalendar() {
    if (this.slideInit) {
      this.slideInit = false;
      this.slideInCalendar = true;
      return;
    }
    this.slideInCalendar = !this.slideInCalendar;
    this.slideOutCalendar = !this.slideOutCalendar;
  }

  myTrainings(dateRef: Date) {
    return new Promise((resolve, reject) => {
      let dateRefIni = new Date(dateRef.getFullYear(), dateRef.getMonth());
      dateRefIni.setDate(dateRefIni.getDate() - dateRefIni.getDay());
      dateRefIni.setHours(0);
      dateRefIni.setMinutes(0);
      dateRefIni.setSeconds(0);
      let dateRefFim = new Date(dateRefIni);
      dateRefFim.setDate(dateRefFim.getDate() + 41);
      dateRefFim.setHours(23);
      dateRefFim.setMinutes(59);
      dateRefFim.setSeconds(59);
      this.trainingProvider.getMyTrainings()
        .then((trainings: Training[]) => {
          return trainings.filter((x: Training) => new Date(x.inicio + " 01:00:00") >= dateRefIni && new Date(x.inicio + " 01:00:00") <= dateRefFim)
            .forEach((trainign: Training) => {
              debugger;
              let trainingDateRefIni = new Date(trainign.inicio + " 01:00:00");
              trainingDateRefIni.getDay() < 6 ? trainingDateRefIni.setDate(trainingDateRefIni.getDate() - trainingDateRefIni.getDay()) : trainingDateRefIni.setDate(trainingDateRefIni.getDate() + 1);
              let trainingDateRefFim = new Date(trainign.fim + " 01:00:00") <= dateRefFim ? new Date(trainign.fim + " 01:00:00") : new Date(dateRefFim);
              let dayFirstMonth = new Date(trainingDateRefIni.getFullYear(), trainingDateRefIni.getMonth());
              let index = trainingDateRefIni.getDate();

              while (trainingDateRefIni <= trainingDateRefFim) {
                let dayRef = 0;
                trainign.frequency.forEach(day => {
                  trainingDateRefIni.setDate(trainingDateRefIni.getDate() + parseInt(day) - dayRef);
                  index = index + parseInt(day) - dayRef;

                  if (trainingDateRefIni <= trainingDateRefFim) {
                    this.month[index]["itens"].push(trainign);
                    this.month[index]["training"] = true;
                  }
                  dayRef = parseInt(day);
                })

                let next = 7 - trainingDateRefIni.getDay();
                trainingDateRefIni.setDate(trainingDateRefIni.getDate() + next);
                index = index + next;
              }
              resolve();
            })
        })
    })
  }

}
