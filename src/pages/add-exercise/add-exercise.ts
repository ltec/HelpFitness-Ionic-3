import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { app, addExercisePage } from './../../config/portugues';
import { PageSettingsProvider } from './../../providers/page-settings/page-settings';
import { FormBuilder, Validators } from '@angular/forms';
import { MessageProvider } from './../../providers/message/message';
import { Exercise } from './../../model/exercise';
import { Serie } from '../../model/serie';

@IonicPage()
@Component({
  selector: 'page-add-exercise',
  templateUrl: 'add-exercise.html',
})
export class AddExercisePage {

  language: any;
  libra: number = 0;
  repetitions: number = 0;
  item: Exercise;
  myForm: any;
  index: number;
  updated: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private pageSettingsProvider: PageSettingsProvider, formBuilder: FormBuilder,
    private events: Events,
    private messageProvider: MessageProvider) {
    this.language = addExercisePage;
    this.language.app = app;
    debugger;
    this.item = this.navParams.get("item");

    this.myForm = formBuilder.group({
      libra: ['', Validators.compose([
        Validators.min(1),
        Validators.max(999),
        Validators.pattern("^[0-9]+$")
      ])],
      repetitions: ['', Validators.compose([
        Validators.min(1),
        Validators.max(999),
        Validators.pattern("^[0-9]+$")])]
    });
  }

  ionViewDidLoad() {
    debugger;
    if (this.navParams.get("updated")) {
      this.index = this.navParams.get("index");
      this.updated = this.navParams.get("updated");
    }
    else{
      this.item.series = [];
    }
  }

  addSerie() {
    this.item.series.push(new Serie(this.repetitions, this.libra));
  }

  updateExercise(item) {
    this.libra = item.libra;
    this.repetitions = item.repetitions;
  }

  deleteExercise(index) {
    this.messageProvider.confirm(this.language.o, this.language.p)
      .then(() => this.item.series.splice(index, 1))
  }

  addExercise() {
    this.pageSettingsProvider.setPageFooter(false)
      .then(() => {
        debugger;
        if (!this.updated) {
          this.events.publish('exercise:created', this.item);
        }
        else {
          this.events.publish('exercise:updated', this.item, this.index);
        }
        this.navCtrl.remove(2, this.navCtrl.length() - 2);
      });
  }

}
