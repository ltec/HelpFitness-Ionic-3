import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddExercisePage } from './add-exercise';
import { HeaderModule } from './../../components/header/header.module';
import { ModalDismissModule } from './../../components/modal-dismiss/modal-dismiss.module';
import { NavPopToRootModule } from './../../components/nav-pop-to-root/nav-pop-to-root.module';

@NgModule({
  declarations: [
    AddExercisePage,
  ],
  imports: [
    HeaderModule,
    NavPopToRootModule,
    IonicPageModule.forChild(AddExercisePage),
  ],
})
export class AddExercisePageModule {}
