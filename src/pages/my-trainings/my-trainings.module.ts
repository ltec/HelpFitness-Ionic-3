import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyTrainingsPage } from './my-trainings';
import { HeaderModule } from './../../components/header/header.module';
import { TrainingProvider } from '../../providers/training/training';
import { DaysInWeekPipeModule } from './../../pipes/days-in-week/days-in-week.module';
import { SerieXrepetitionsPipeModule } from '../../pipes/serie-xrepetitions/serie-xrepetitions.module';
import { LongPressModule } from './../../directives/long-press/long-press.module';
import { CardTrainingModule } from './../../components/card-training/card-training.module';

@NgModule({
  declarations: [
    MyTrainingsPage,
  ],
  imports: [
    HeaderModule,
    LongPressModule,
    DaysInWeekPipeModule,
    SerieXrepetitionsPipeModule,
    CardTrainingModule,
    IonicPageModule.forChild(MyTrainingsPage),
  ],
  providers: [
    TrainingProvider
  ]
})
export class MyTrainingsPageModule { }
