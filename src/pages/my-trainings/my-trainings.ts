import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { myTrainingsPage } from './../../config/portugues';
import { MessageProvider } from './../../providers/message/message';
import { LoadingProvider } from './../../providers/loading/loading';
import { TrainingProvider } from './../../providers/training/training';
import { Training } from '../../model/training';

@IonicPage()
@Component({
  selector: 'page-my-trainings',
  templateUrl: 'my-trainings.html',
})
export class MyTrainingsPage {
  language: any;
  myTrainings: Training[] = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private messageProvider: MessageProvider,
    private loadingProvider: LoadingProvider,
    private trainingProvider: TrainingProvider,
    private events: Events) {
    this.language = myTrainingsPage;

    this.events.subscribe('training:created', (training) => {
      this.myTrainings.push(training);
      this.trainingProvider.setMyTrainings(this.myTrainings)
        .then(() => this.events.publish('training:add'))
        .catch(error => this.messageProvider.errorMessage(error));
    });

    this.events.subscribe('training:updated', (training, index) => {
      this.myTrainings[index] = training;
      this.trainingProvider.setMyTrainings(this.myTrainings)
        .then(() => this.events.publish('training:add'))
        .catch(error => this.messageProvider.errorMessage(error));
    });
  }

  ionViewDidLoad() {
    this.getMyTrainings();
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  getMyTrainings() {
    this.trainingProvider.getMyTrainings()
      .then((trainings: any[]) => {
        this.myTrainings = trainings;
      })
      .catch(error => this.messageProvider.errorMessage(error));
  }

  addTraining() {
    this.navCtrl.push("AddTrainingPage");
  }

  deleteTraining({ index }) {
    debugger;
    this.messageProvider.confirm(this.language.o, this.language.p)
      .then(() => {
        this.myTrainings.splice(index, 1);
        this.trainingProvider.setMyTrainings(this.myTrainings)
      })
  }

  updateTraining({ training, index }) {
    debugger;
    this.navCtrl.push("AddTrainingPage", { item: training, index: index, updated: true })
  }

}
