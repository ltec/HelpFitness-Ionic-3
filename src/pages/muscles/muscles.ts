import { Component } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { musclePage } from './../../config/portugues';

@IonicPage()
@Component({
    selector: 'page-muscles',
    templateUrl: 'muscles.html',
})
export class MusclesPage {
    source = [
        {
            "key": 1,
            "muscle": "Peito",
            "description": "Supino reto, supino inclinado, cross-over...",
            "image": "peito",
            "exercises": []
        },
        {
            "key": 2,
            "muscle": "Costas",
            "description": "Pulley costa, remada aberta, remada curvada...",
            "image": "costas"
        },
        {
            "key": 3,
            "muscle": "Ombros",
            "description": "Elevação lateral, desenvolvimento com halteres, remada alta...",
            "image": "ombros"
        },
        {
            "key": 4,
            "muscle": "Biceps",
            "description": "Rosca direta, rosca martelo, rosca concentrada...",
            "image": "biceps"
        },
        {
            "key": 5,
            "muscle": "Triceps",
            "description": "Triceps francês unilateral, triceps corda, tríceps pulley...",
            "image": "triceps"
        },
        {
            "key": 6,
            "muscle": "Pernas",
            "description": "Leg-press, agachamento, extensor...",
            "image": "pernas"
        },
        {
            "key": 7,
            "muscle": "Abdominais",
            "description": "Abdominal infra, abdominal supra, abdominal na bola...",
            "image": "abdominais"
        }
    ];
    language: any;

    constructor(public navCtrl: NavController, private events: Events) {
        this.language = musclePage;
    }

    ionViewDidLoad() {

    }

    selectMuscle(item) {
        this.events.publish('muscle:selected', item);
        this.navCtrl.popToRoot()
    }

}
