import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MusclesPage } from './muscles';
import { HeaderModule } from './../../components/header/header.module';

@NgModule({
  declarations: [
    MusclesPage,
  ],
  imports: [
    HeaderModule,
    IonicPageModule.forChild(MusclesPage),
  ],
})
export class MusclesPageModule {}
