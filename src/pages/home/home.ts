import { Component, AfterViewInit } from '@angular/core';
import { NavController, IonicPage, NavParams } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MessageProvider } from './../../providers/message/message';
import { LoadingProvider } from './../../providers/loading/loading';
import { TrainingProvider } from './../../providers/training/training';
import { homePage } from './../../config/portugues';
import { PageSettingsProvider } from './../../providers/page-settings/page-settings';
import { Exercise } from './../../model/exercise';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements AfterViewInit {

  source: any;
  language: any;
  pageFooterShow: boolean = false;
  goal: number = 1;
  muscles: any = [1, 2, 3, 4, 5, 6, 7];

  constructor(public navCtrl: NavController,
    private splashScreen: SplashScreen,
    private messageProvider: MessageProvider,
    private loadingProvider: LoadingProvider,
    private trainingProvider: TrainingProvider,
    private pageSettingsProvider: PageSettingsProvider) {
    this.language = homePage;
    this.loadingProvider.loadingWithMessage("Loading...");
    this.trainingProvider.getTrainingsAll(this.goal, this.muscles)
      .then(source => this.source = source)
      .catch(error => this.messageProvider.errorMessage(error.message | error))
      .then(() => this.loadingProvider.dismiss());
    this.pageFooterShow = this.pageSettingsProvider.footer;
  }

  ionViewDidLoad() {

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.splashScreen.hide();
    }, 2000);
  }

  getTraining({ item }) {
    if (!item)
      return;
    this.navCtrl.push("TrainingDetailPage",
      {
        item: new Exercise(
          item.key,
          item.image,
          item.exercise,
          [],
          item.musculosSolicitados,
          item.execucao
        )
      });
  }

  filter(){
    this.navCtrl.push("GoalsPage")
  }






}
