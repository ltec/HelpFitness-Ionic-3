import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { CatalogModule } from '../../components/catalog/catalog.module';
import { TrainingProvider } from './../../providers/training/training';
import { HeaderModule } from './../../components/header/header.module';
import { NavPopToRootModule } from './../../components/nav-pop-to-root/nav-pop-to-root.module';
import { MuscleFilterPipeModule } from './../../pipes/muscle-filter/muscle-filter.module';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    CatalogModule,
    HeaderModule,
    NavPopToRootModule,
    MuscleFilterPipeModule,
    IonicPageModule.forChild(HomePage),
  ],
  exports: [
    HomePage
  ],
  providers: [
    TrainingProvider
  ]
})
export class HomePageModule { }
