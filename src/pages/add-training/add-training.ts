import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { app, addTrainingPage } from './../../config/portugues';
import { FormBuilder, Validators } from '@angular/forms';
import { PageSettingsProvider } from './../../providers/page-settings/page-settings';
import { TrainingProvider } from './../../providers/training/training';
import { MessageProvider } from './../../providers/message/message';
import { Training } from './../../model/training';
import { Exercise } from './../../model/exercise';

@IonicPage()
@Component({
  selector: 'page-add-training',
  templateUrl: 'add-training.html',
})
export class AddTrainingPage {
  language: any;
  myForm: any;
  item: Training;
  index: number;
  state;
  updated: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    formBuilder: FormBuilder,
    private pageSettingsProvider: PageSettingsProvider,
    private events: Events,
    private trainingProvider: TrainingProvider,
    private messageProvider: MessageProvider) {
    this.language = addTrainingPage;
    this.language.app = app;
    this.item = new Training();
    this.myForm = formBuilder.group({
      title: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(20),
      Validators.required])],
      inicio: ['', Validators.compose([Validators.required])],
      fim: ['', Validators.compose([Validators.required])],
      frequencia: ['', Validators.compose([Validators.required])],
    });

    this.events.subscribe('exercise:created', (exercise) => {
      this.item.exercises.push(exercise);
    });

    this.events.subscribe('exercise:updated', (exercise: Exercise, index) => {
      this.item.exercises.splice(index, 1,
        new Exercise(
          exercise.key,
          exercise.image,
          exercise.exercise,
          exercise.series,
          exercise.musculosSolicitados,
          exercise.execucao));
    });
  }

  ionViewDidLoad() {
    if (this.navParams.get("updated")) {
      this.item = this.navParams.get("item");
      this.index = this.navParams.get("index");
      this.updated = this.navParams.get("updated");
    }
  }

  ionViewDidEnter() {

  }

  ionViewWillLeave() {

  }

  Save() {
    if (!this.updated) {
      this.events.publish('training:created', this.item);
    }
    else {
      this.state = this.item;
      this.events.publish('training:updated', this.item, this.index);
    }

    this.navCtrl.pop()
  }

  addExercise() {
    this.pageSettingsProvider.setPageFooter(true)
      .then(() => this.navCtrl.push("HomePage"));
  }

  updateExercise(item, index) {
    this.pageSettingsProvider.setPageFooter(true)
      .then(() => this.navCtrl.push("AddExercisePage", { item: item, index: index, updated: true }));
  }

  deleteExercise(index) {
    this.messageProvider.confirm(this.language.o, this.language.p)
      .then(() => this.item.exercises.splice(index, 1))
  }

}
