import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddTrainingPage } from './add-training';
import { HeaderModule } from './../../components/header/header.module';
import { SerieXrepetitionsPipeModule } from '../../pipes/serie-xrepetitions/serie-xrepetitions.module';
import { TrainingProvider } from '../../providers/training/training';
import { LongPressModule } from './../../directives/long-press/long-press.module';

@NgModule({
  declarations: [
    AddTrainingPage,
  ],
  imports: [
    SerieXrepetitionsPipeModule,
    HeaderModule,
    LongPressModule,
    IonicPageModule.forChild(AddTrainingPage),
  ],
  providers: [
    TrainingProvider
  ]
})
export class AddTrainingPageModule {}
