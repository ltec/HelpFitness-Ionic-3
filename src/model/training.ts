import { Exercise } from "./exercise";

export class Training {
    title: string;
    inicio: Date;
    fim: Date;
    frequency: string[];
    exercises: Exercise[];

    constructor(title: string = '',
        inicio: Date = null,
        fim: Date = null,
        frequency: string[] = [],
        exercises: Exercise[] = []) {
        this.title = title;
        this.inicio = inicio;
        this.fim = fim;
        this.frequency = frequency;
        this.exercises = exercises;

    }
}