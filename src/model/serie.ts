export class Serie {
    repetitions: number;
    libra: number;

    constructor(repetitions: number = 0, libra: number = 0) {
        this.repetitions = repetitions ;
        this.libra = libra;
        
    }
}