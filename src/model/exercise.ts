import { Serie } from './serie';

export class Exercise {
    key: number;
    image: string;
    exercise: string;
    series: Serie[];
    musculosSolicitados: string[];
    execucao: string;

    constructor(key: number = 0,
        image = '',
        exercise: string = '',
        series: Serie[] = [],
        musculosSolicitados: string[],
        execucao: string = '') {
        this.key = key;
        this.image = image;
        this.exercise = exercise;
        this.series = series;
        this.musculosSolicitados = musculosSolicitados;
        this.execucao = execucao;
    }
}