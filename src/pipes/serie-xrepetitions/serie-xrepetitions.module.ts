import { NgModule} from '@angular/core';
import { SerieXrepetitionsPipe } from './serie-xrepetitions';

@NgModule({
	declarations: [
        SerieXrepetitionsPipe
    ],
	imports: [],
    exports: [SerieXrepetitionsPipe],
    })
export class SerieXrepetitionsPipeModule {}