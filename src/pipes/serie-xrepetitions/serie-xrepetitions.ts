import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SerieXrepetitionsPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'serieXrepetitions',
})
export class SerieXrepetitionsPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(source: any[]) {
    if (!source)
      return;

    return source.map((x, index) => (index + 1) + "° " + x.repetitions + 'x' + x.libra).join(', ');
  }
}
