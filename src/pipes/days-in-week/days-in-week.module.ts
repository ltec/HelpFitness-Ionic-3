import { NgModule} from '@angular/core';
import { DaysInWeekPipe } from './days-in-week';

@NgModule({
	declarations: [
        DaysInWeekPipe
    ],
	imports: [],
    exports: [DaysInWeekPipe],
    })
export class DaysInWeekPipeModule {}