import { Pipe, PipeTransform } from '@angular/core';
import { app } from './../../config/portugues';
/**
 * Generated class for the SerieXrepetitionsPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'daysInWeek',
})
export class DaysInWeekPipe implements PipeTransform {
  language: any;
  transform(source: any[]) {
    debugger;
    this.language = app;
    const result = source.map(x => {
      switch (x) {
        case "1":
          return this.language.f
        case "2":
          return this.language.g
        case "3":
          return this.language.h
        case "4":
          return this.language.i
        case "5":
          return this.language.j
        case "6":
          return this.language.k
        case "7":
          return this.language.l
        default:
          return x
      }
    }).join(" ");
    debugger;
    return result;
  }
}
