import { NgModule} from '@angular/core';
import { MuscleFilterPipe } from './muscle-filter';

@NgModule({
	declarations: [
        MuscleFilterPipe
    ],
	imports: [],
    exports: [MuscleFilterPipe],
    })
export class MuscleFilterPipeModule {}