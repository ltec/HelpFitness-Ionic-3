import { Pipe, PipeTransform } from '@angular/core';
import { app } from './../../config/portugues';
/**
 * Generated class for the SerieXrepetitionsPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'muscleFilter',
})
export class MuscleFilterPipe implements PipeTransform {
  language: any;
  transform(source: any[], key: number) {
    if (!source)
      return;

      console.log(source);
    return source.find(x => x.key == key).exercises
  }
}
